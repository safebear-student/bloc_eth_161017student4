pragma solidity ^0.4.17;

contract Safevote {

    mapping (bytes32 => uint8) public votesReceived;

    bytes32[] public canditateList;

    function Safevote(bytes32[] canditateNames) {
        canditateList = canditateNames;
    }

    function voteForCandidate(bytes32 candidate) {
        require (validCandidate(candidate) == true);
        votesReceived[candidate] += 1;
    }

    function totalVotesFor(bytes32 candidate) returns (uint8) {
        require (validCandidate(candidate) == true);
        return votesReceived[candidate];
    }

    function validCandidate(bytes32 candidate) returns (bool) {
        for(uint8 i = 0; i < canditateList.length; i++) {
            if (canditateList[i] == candidate) {
                return true;
            }
        }
        return false;
    }
}
